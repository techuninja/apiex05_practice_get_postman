package com.techu.productos;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductosController {

    @GetMapping("/v0/productos/{id}")
    public Producto getProductoId1(@PathVariable Integer id){
        Producto producto = new Producto(id);
        return producto;
    }

    @GetMapping("/v1/productos")
    public Producto getProductoId2(@RequestParam(value = "id", defaultValue = "0") Integer id){
        Producto producto = new Producto(id);
        return producto;
    }
}
